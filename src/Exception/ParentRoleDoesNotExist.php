<?php declare(strict_types = 1);

namespace Kirill\Greets\Deputy\Exception;

class ParentRoleDoesNotExist extends RoleDoesNotExist
{
}

