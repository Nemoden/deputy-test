<?php declare(strict_types = 1);

namespace Kirill\Greets\Deputy\Exception;

use Exception;

class UserDoesNotExist extends Exception
{
}

