<?php declare(strict_types = 1);

namespace Kirill\Greets\Deputy;

use Kirill\Greets\Deputy\Exception\ParentRoleDoesNotExist;
use Kirill\Greets\Deputy\Exception\RoleDoesNotExist;
use Kirill\Greets\Deputy\Exception\UserDoesNotExist;

class Hierarchy
{
    /**
     * @var array
     */
    protected $parentRoles = [];

    /**
     * @var array
     */
    protected $roleSubordinates = [];

    /**
     * @var array
     */
    protected $usersByRole = [];

    /**
     * @var array
     */
    protected $usersById = [];

    /**
     * @var array
     */
    protected $users = [];

    /**
     * Sets a list of roles.
     *
     * @param array $roles
     */
    public function setRoles(array $roles)
    {
        foreach ($roles as $role) {
            $this->addRole($role);
        }
    }

    /**
     * @param array $users
     *
     * @throws RoleDoesNotExist
     */
    public function setUsers(array $users)
    {
        foreach ($users as $user) {
            $this->addUser($user);
        }
    }

    /**
     * @param array $user
     *
     * @throws RoleDoesNotExist
     */
    protected function addUser(array $user)
    {
        $this->ensureRole((int)$user['Role']);
        $this->usersByRole[$user['Role']][] = $user;
        $this->usersById[$user['Id']] = $user;
    }

    /**
     * @param array $role
     *
     * @throws ParentRoleDoesNotExist
     */
    protected function addRole(array $role): void
    {
        $id = $role['Id'];
        $parentId = $role['Parent'];
        $name = $role['Name'];
        if ($parentId !== 0 && !isset($this->parentRoles[$parentId])) {
            throw new ParentRoleDoesNotExist("Parent role id {$parentId} does not exist on the system");
        }

        // No need to populate root.
        if ($id !== 0) {
            $this->parentRoles[$id] = $parentId;
        }

        while (isset($this->parentRoles[$parentId])) {
            if (!isset($this->roleSubordinates[$parentId])) {
                $this->roleSubordinates[$parentId] = [];
            }
            $this->roleSubordinates[$parentId][] = $id;
            // "Bubble up" role into parents.
            $parentId = $this->parentRoles[$parentId];
        }
    }

    /**
     * @param int $userId
     */
    public function getSubOrdinates(int $userId)
    {
        $user = $this->getUserById($userId);
        $subordinateRoles = $this->getSubordinateRoles((int)$user['Role']);
        $subordinateUsers = [];
        foreach ($subordinateRoles as $subRole) {
            array_push($subordinateUsers, ...$this->getUsersByRole($subRole));
        }
        return $subordinateUsers;
    }

    /**
     * @param int $userId
     * @throws UserDoesNotExist
     * @return array
     */
    protected function getUserById(int $userId): array
    {
        if (!isset($this->usersById[$userId])) {
            throw new UserDoesNotExist("User id {$userId} does not exist on the system");
        }
        return $this->usersById[$userId];
    }

    /**
     * Returns a list of subordinate roles.
     *
     * @param int $roleId
     *
     * @throws RoleDoesNotExist
     */
    protected function getSubordinateRoles(int $roleId)
    {
        $this->ensureRole($roleId);
        return $this->roleSubordinates[$roleId] ?? [];
    }

    /**
     * @throws RoleDoesNotExist
     */
    protected function ensureRole(int $roleId)
    {
        // It's legit to use $this->parentRoles to check if user exists
        // since all the users on addition will be added to that index.
        if (!isset($this->parentRoles[$roleId])) {
            throw new RoleDoesNotExist("Role id {$roleId} does not exist on the system");
        }
    }

    /**
     * @param int $roleId
     */
    protected function getUsersByRole(int $roleId): array
    {
        return $this->usersByRole[$roleId] ?? [];
    }
}

