help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: test
test: ## Runs unit tests
	@vendor/bin/phpunit tests

.PHONY: install
install: ## Installs dependencies
	@./composer install

docker-install: ## Installs dependencies using docker
	@docker run -v `pwd`/:/deputy-test -w /deputy-test --entrypoint=php php:7.4-cli /deputy-test/composer install

docker-test: ## Runs unit tests using docker
	@docker run -v `pwd`/:/deputy-test -w /deputy-test --entrypoint=/deputy-test/vendor/bin/phpunit php:7.4-cli tests
