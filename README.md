## How to run

Run `make` in the root for instructions.

### Local php

Precondition: You need a modern(-ish) version of php (mine is 7.4). It can be obtained through various methods: package manager (apt, yum, pacman, etc.), php package managers (such as [`phpbrew`](https://github.com/phpbrew/phpbrew)), compiling from sources, etc.

Once you have php on your system, just run

```
make install
```

In order to test, run:

```
make test
```

### Docker

If you don't have php on your system, don't want to install it, or your php version is different, you can also use [`docker`](https://docs.docker.com/engine/install/). Same commands, but with docker are:

```
make docker-install
```

and

```
make docker-test
```

## Thoughts on the problem

As we have a roles hierarchy we are not expecting it to be very large (i.e. it's unimaginable to have 1000s of roles).
I also wouldn't expect roles hierarchy to be changed much, but I would expect we'd want to access it often.

**Conclusion** We'd want to optimise for reads (i.e. getting subordinate roles should be fast), but we can sacrifice writes speed (i.e. if the addition of a new role is slow that's fine).

## Thoughts on implementation

### OOP I didn't go with

At first sight, it makes sense to make models for Role and User, i.e. something like

```
class Role {
    protected $id;
    protected $parentId;
    protected $name;
    public function getId() {...}
    public function getParentId() {...}
    public function getName() {...}
}
```

but I would expect we are going after algorithmic solution more than OOP modeling solution, so I will keep it simple and those models may be added into the system at any point.

### Solution I didn't go with

At first sight, this problem is about building a tree such as

```
                           Role<0>
                             |
   +-------------------------+----------------------------+
   |                         |                            |
Role<1>                    Role<2>                     Role<3>
                             |                            |
                             |                            |
                      +------+------+                  Role<6>
                      |             |
                  Role<4>         Role<5>
                                    |
                                    |
                                  Role<7>
```

And then getting subordinates of `Role<2>` would be finding it in the tree, and getting all the children recursively using a tree traversal algorithm.
But, this tree traversal on reads will require at least `O(N)` time if children are stored in something like a linked list whereas N is a number of children.
This would apply both for traversal and searching for a node. If we want to reduce complexity we'd need to maintain some extra data such as a path to a specific node,
i.e. for `Role<7>` we'd need to maintain a path such as `[Role<0>, Role<2>, Role<5>]`. We could also store children as a hashmap so that accessing a child is fast.
This implementation becomes complex.

### Solution I've implemented

I spotted that we can do getting all the subordinates of any role in `O(1)` for any role. This is achievable if we maintain a dictionary of `Role => List<Role>` whereas `List<Role>` is a list of subordinate roles.

For the above example, that'd be something like

```
0 => [], // no need to populate root
1 => [],
2 => [4, 5, 7],
3 => [6],
4 => [],
5 => [7],
6 => [],
7 => [],
```

Now we can get subordinates of any role in `O(1)`. For that, we sacrifice both writes speed and space which I believe is a reasonable trade-off provided that there won't be many writes and the number of roles in the system will be relatively low (again, I can not imagine a system with 1000s of roles, in most systems that I worked with the number of roles was usually limited to maybe a dozen).

We also need to index users by role, i.e. provided we have `UserA<Role<1>>`, `UserB<Role<2>>`, and `UserC<Role<3>>`, the index that we want to maintain is

```
1 => [UserA, UserC],
2 => [UserB],
```

so that as soon as we have determined all the subordinate roles, we can easily pull down all the users assigned to these roles.
