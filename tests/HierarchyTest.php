<?php declare(strict_types = 1);

namespace Test;

use Kirill\Greets\Deputy\Exception\ParentRoleDoesNotExist;
use Kirill\Greets\Deputy\Exception\RoleDoesNotExist;
use Kirill\Greets\Deputy\Exception\UserDoesNotExist;
use Kirill\Greets\Deputy\Hierarchy;
use PHPUnit\Framework\TestCase;

class HierarchyTest extends TestCase
{
    public function testNoUsersProvidedYieldsExceptionWhenGettingSubordinates()
    {
        $this->expectException(UserDoesNotExist::class);
        // There are no users provided, so we can not get subordinates of any user.
        // It's up to us how to treat this situation, - whether it's an exception or
        // getSubOrdinates method simply returns an empty result.
        $hierarchy = new Hierarchy;
        $hierarchy->getSubOrdinates(1); // user id 1 does not exist.
    }

    public function testSettingUsersBeforeRolesAreKnownFails()
    {
        $hierarchy = new Hierarchy;
        [$roles, $users] = $this->loadFixture('providedExample');
        $this->expectException(RoleDoesNotExist::class);
        // No roles are provided yet, so adding a users must fail since their role will be unknown.
        $hierarchy->setUsers($users);
    }

    public function testIncorrectUserRole()
    {
        $hierarchy = new Hierarchy;
        [$roles, $users] = $this->loadFixture('incorrectUserRole');
        // No roles are provided yet, so adding a users must fail since their role will be unknown.
        $hierarchy->setRoles($roles);
        // incorrectUserRole has a user with a role of 15 which doesn't exist on the system.
        $this->expectException(RoleDoesNotExist::class);
        $hierarchy->setUsers($users);
    }

    public function testProvidedExample()
    {
        [$roles, $users] = $this->loadFixture('providedExample');
        $hierarchy = new Hierarchy;
        $hierarchy->setRoles($roles);
        $hierarchy->setUsers($users);
        $expectedSubordinates = [
            [ "Id" => 2, "Name" => "Emily Employee", "Role" => 4, ],
            [ "Id" => 5, "Name" => "Steve Trainer", "Role" => 5, ],
        ];
        $this->assertSubordinatesSame($expectedSubordinates, $hierarchy->getSubOrdinates(3));
        $expectedSubordinates = [
            ["Id"=> 2, "Name" => "Emily Employee", "Role" => 4, ],
            ["Id"=> 3, "Name" => "Sam Supervisor", "Role" => 3, ],
            ["Id"=> 4, "Name" => "Mary Manager", "Role" => 2, ],
            ["Id"=> 5, "Name" => "Steve Trainer", "Role" => 5, ],
        ];
        $this->assertSubordinatesNotSame($expectedSubordinates, $hierarchy->getSubOrdinates(3));
        $this->assertSubordinatesSame($expectedSubordinates, $hierarchy->getSubOrdinates(1));
        $this->assertSubordinatesSame([], $hierarchy->getSubOrdinates(5));
    }

    public function testRolesHierarchyCorrectness()
    {
        $hierarchy = new Hierarchy();
        [$roles, $users] = $this->loadFixture('incorrectRolesHierarchy');
        $this->expectException(ParentRoleDoesNotExist::class);
        $hierarchy->setRoles($roles);
    }

    protected function assertSubordinates(array $expected, array $actual, bool $shouldBeSame)
    {
        $sortSubordinates = function(array $subordinates) {
            usort($subordinates, function ($a, $b) {
                return $a['Id'] < $b['Id'];
            });
            return $subordinates;
        };
        $method = $shouldBeSame ? 'assertSame' : 'assertNotSame';
        $this->{$method}(
            $sortSubordinates($expected),
            $sortSubordinates($actual)
        );
    }

    protected function assertSubordinatesSame($expected, $actual)
    {
        $this->assertSubordinates($expected, $actual, $shouldBeSame = true);
    }

    protected function assertSubordinatesNotSame($expected, $actual)
    {
        $this->assertSubordinates($expected, $actual, $shouldBeSame = false);
    }

    protected function loadFixture(string $name)
    {
        $contents = file_get_contents(__DIR__ . '/fixtures/' . $name . '.json');
        $fixture = json_decode($contents, true);
        return [$fixture['roles'], $fixture['users']];
    }
}
